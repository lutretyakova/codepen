window.onload = () => {
   const triangle = document.querySelector('.triangle')
   for (let inx = 0; inx < 7; inx++) {
       let newTriangle = triangle.cloneNode(true);
       newTriangle.classList.remove('gr-1')
       newTriangle.classList.add(`gr-${inx + 2}`)
       document.querySelector('svg').appendChild(newTriangle);
   }
}