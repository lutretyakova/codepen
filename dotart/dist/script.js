const PENCIL = 'pencil'
const ERASER = 'eraser'
const DOT_COUNT = 10
const DOT_SIZE = 10
const PALETTE = {
    'navy': '#001f3f',
    'blue': '#0074D9',
    'aqua': '#7FDBFF',
    'teal': '#39CCCC',
    'olive': '#3D9970',
    'green': '#2ECC40',
    'lime': '#01FF70',
    'yellow': '#FFDC00',
    'orange': '#FF851B',
    'red': '#FF4136',
    'maroon': '#85144b',
    'fuchsia': '#F012BE',
    'purple': '#B10DC9',
    'black': '#000',
    'gray': '#AAA',
    'silver': '#EEE'
}

let mode = PENCIL
let currentColor = 'aqua'

window.onload = () => {
    document.querySelector('.pencil').onclick = changeMode
    document.querySelector('.eraser').onclick = changeMode
    document.querySelector('.save').onclick = saveToFile

    for(let row = 0; row < DOT_COUNT; row++){
        for (let col = 0; col < DOT_COUNT; col++) {
            const block = document.createElement('div')
            block.className = `circle dot dot-${row}-${col}`
            block.onclick = fillDot
            document.querySelector('.draw-area').appendChild(block)
        }
    }

    for(let key of Object.keys(PALETTE)){
        const li = document.createElement('li')
        li.className = `circle color ${ key }`
        li.onclick = changeColor
        document.querySelector('.color-menu').appendChild(li)
    }

    document.querySelector(`.${currentColor}`).classList.add('current')

    fillCanvas()
}

const fillCanvas = () => {
    const canvas = document.querySelector('canvas')
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = '#333'
    ctx.fillRect(0, 0, canvas.width, canvas.height)
}

const changeMode = (event) => {
    if (event.target.className.includes(PENCIL)) {
        document.querySelector('.pencil').classList.add('active')
        document.querySelector('.eraser').classList.remove('active')
        mode = PENCIL
        return
    }

    if (event.target.className.includes(ERASER)) {
        document.querySelector('.eraser').classList.add('active')
        document.querySelector('.pencil').classList.remove('active')
        mode = ERASER
    }
}

const fillDot = (event) => {
    const color = event.target.className.replace(/circle|dot-\d-\d|dot|\s+/g, '')
    let fillColor = 'transparent'
    if (color) {
        event.target.classList.remove(color)
    }
    
    if (mode === PENCIL) {
        event.target.classList.add(currentColor)
        fillColor = PALETTE[currentColor]
    }

    const cellInfo = /(\d)-(\d)/g.exec(event.target.className)
    const [row, col] = [cellInfo[1], cellInfo[2]]
    
    const x = col * DOT_SIZE + DOT_SIZE / 2
    const y = row * DOT_SIZE + DOT_SIZE / 2

    const canvas = document.querySelector('canvas')
    const ctx = canvas.getContext('2d')
    
    ctx.beginPath()
    ctx.fillStyle = '#333'
    ctx.fillRect(x - 5, y - 5, DOT_SIZE, DOT_SIZE)
    ctx.fillStyle = fillColor
    ctx.arc(x, y, DOT_SIZE / 2, 0, 2 * Math.PI, false)
    ctx.fill()
}

const changeColor = (event) => {
    document.querySelector(`.color-menu .${ currentColor }`).classList.remove('current')

    const color = event.target.className.replace(/circle|color|\s+/g, '')
    currentColor = color
    event.target.classList.add('current')
}

const saveToFile = () => {
    const canvas = document.querySelector('canvas')
    const image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream")
    window.location.href = image 
}