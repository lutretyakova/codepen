const MAIN_URL = "https://icanhazdadjoke.com/search?page="
const VIEW_SIZE = 6
let current = 1
let pageRequest = 1
let content = ""
let term = ""

window.onload = async() => {
    content = await getInfo(`${MAIN_URL}${ pageRequest }`)
    content = content.results
    pageRequest = content['next_page']
    addInfo(content)

    document.querySelector(".more").addEventListener("click", loadMore)
    document.querySelector("form").addEventListener("submit", searchJokes)
}

const loadMore = async() => {
    current += 1
    const nextPage = content.slice(VIEW_SIZE * (current - 1), VIEW_SIZE * current)

    if ((current + 1) * VIEW_SIZE > content.length) {
        const nextContent = await getInfo(`${MAIN_URL}${ pageRequest }`)
        content = content
            .slice(VIEW_SIZE * current, content.length)
            .concat(nextContent.results)
        pageRequest = content['next_page']
        current  = 0
    }
    const list = document.querySelector(".jokes")
    list.innerHTML = ""

    addInfo(nextPage)
}

const searchJokes = async() => {
    event.preventDefault();
    pageRequest = 1
    const searchInput = document.querySelector("form input")
    term = searchInput.value
    content = await getInfo(`${MAIN_URL}${ pageRequest }&term=${term}`)
    content = content.results
    pageRequest = content['next_page']

    const list = document.querySelector(".jokes")
    list.innerHTML = ""
    searchInput.value = ""
    document.querySelector("button").focus()
  
    addInfo(content)
}

const getInfo = async(url) => {
    return new Promise((resolve) => 
        fetch(url, 
            {
                headers : {
                    "Accept": "application/json"}
            })
            .then((info) => {
                resolve(info.json())
        })
    )
}

const addInfo = info => {
    info.slice(0, VIEW_SIZE).forEach (
        item => addBlock(item)
    )
}

const addBlock = (item) => {
    const block = document.createElement('li')
    block.className = 'joke-item'
    block.innerHTML = item.joke
    document.querySelector(".jokes").appendChild(block);
}