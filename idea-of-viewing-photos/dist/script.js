let areaLeft = 0
let areaTop = 0

let clickPosX = 0
let clickPosY = 0

let dragging = false
let areaBlock
let width = 0
let height = 0

window.onload = () => {
    height = document.body.clientHeight
    width = document.body.clientWidth
    areaBlock = document.querySelector('.area')

    areaTop = areaBlock.offsetTop
    areaLeft = areaBlock.offsetLeft

    document.querySelector('.area').style.cssText = `
        background-size: ${ width }px ${ height }px;
        background-position: ${ -areaLeft }px ${ -areaTop }px
    `
    areaBlock.onmousemove = handleMouseMove
    areaBlock.onmousedown = handleMouseDown
    areaBlock.onmouseup = handleMouseUp
    areaBlock.onmouseleave = handleMouseUp
}

const handleMouseDown = (event) => {
    clickPosX = event.clientX - areaLeft
    clickPosY = event.clientY - areaTop

    dragging = true
}

const handleMouseMove = (event) => {
    if ( dragging ) {
        let offsetX = event.pageX - areaLeft - clickPosX
        let offsetY = event.pageY - areaTop - clickPosY
        areaLeft += offsetX
        areaTop += offsetY
    
        areaBlock.style.cssText = `
            left: ${ areaLeft }px;
            top: ${ areaTop }px;
            background-size: ${ width }px ${ height }px;
            background-position: ${ -areaLeft }px ${ -areaTop }px
        `
    }
    return false
}

const handleMouseUp = () => {
    dragging = false

    clickPosX = 0
    clickPosY = 0
}