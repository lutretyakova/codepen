const KNIFE = 'knife'
const DONUT = 'donut'

const SAMPLES = [KNIFE, DONUT]
const SAMPLE_IN_LINE = 7
const currentLine = []

let timer
let timerInterval = 2500
let gameLine
let timeUntil = 30
let score = 0
let fail = 0
let total = 0

window.onload = () => {
    gameLine = document.querySelector('.game-line')
    generateLine()

    document.querySelector('.modal').onmousedown = startGame
}

const startGame = () => {
    document.querySelector('.modal').classList.add('hide')
    timer = setInterval(shiftLine, timerInterval)
    gameTimer = setInterval(updateTime, 1000)
}

const generateLine = () => {
    for (let inx = 0; inx < SAMPLE_IN_LINE; inx++) {
        const block = document.createElement('div')
        const typeInx = Math.round(Math.random())
        const type = SAMPLES[typeInx]
        if (type === KNIFE) {
            total++
        }
        currentLine.push(type)
        block.classList.add(type)
        block.classList.add('block')
        block.onmousedown = () => {
            block.classList.add('selected')
            checkCorrect(type)
        }
        gameLine.appendChild(block)
    }

    document.querySelector('.total').innerHTML = `Total: ${total}`
}

const shiftLine = () => {
    generateLine()
    gameLine.classList.add('shift')

    setTimeout(() => {
        currentLine.splice(0, SAMPLE_IN_LINE, [])
        for (let inx = 0; inx < SAMPLE_IN_LINE; inx++) {
            const block = document.querySelector('.block')
            gameLine.removeChild(block)
        }
        gameLine.classList.remove('shift')
    }, 1000)
}

const updateTime = () => {
    timeUntil--

    document.querySelector('.timer').innerHTML =
        `00:` + `00${timeUntil}`.slice(-2)
    if (timeUntil === 0) {
        clearInterval(timer)
        clearInterval(gameTimer)
        document.querySelector('.status').innerHTML = `Game over. Check results.`
        return
    }
}

const checkCorrect = (type) => {
    if (type === KNIFE) {
        score++
        document.querySelector('.score').innerHTML = `Correct: ${score}`
    } else {
        fail++
        document.querySelector('.fail').innerHTML = `Wrong: ${fail}`
    }
}