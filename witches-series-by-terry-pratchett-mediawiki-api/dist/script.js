let titleBook = ''
let urlTemplate = `https://en.wikipedia.org/w/api.php?origin=*&action=query&titles=titleBook&prop=revisions&rvprop=content&format=json&rvsection=1`


window.onload = () => {
    loadFirstWidow()
}

const loadFirstWidow = () => {
    // icon form https://www.iconfinder.com/tobiasehlig
    const dataUri = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUxMiA1MTIiIGhlaWdodD0iNTEycHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEycHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxwYXRoIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTM4MC43NDUsMC4wMDhjMzguODk2LTAuMTIzLDcwLjc1NSwzMS4yMTUsNzEuMDUsNjkuODgyICBjMC4yOTUsMzkuMjAyLTMxLjgyNSw3MS43Mi03MC45NDksNzEuODM3Yy0zOS40MDUsMC4xMTgtNzAuNjc1LTMxLjY0LTcwLjYzNS03MS43MzJDMzEwLjI1MSwzMC45OTUsMzQxLjM1NSwwLjEzOCwzODAuNzQ1LDAuMDA4eiIgZmlsbC1ydWxlPSJldmVub2RkIi8+PHBhdGggZD0iTTUwMS40MywzMTQuNTQ5Yy01LjI3NSwyLjA3LTkuMzksMy42ODktMTUuNDg1LDYuMDhjMy44MjUtNC4zNCw2LjA2NS02Ljg4LDcuNjc2LTguNzA1ICBjLTkuOTQsNC4wMjQtMjAuMzkxLDguMjYtMzAuODUxLDEyLjQ5NWMtMC41NzQtMS4xOTUtMS4xNDktMi4zOTYtMS43MjktMy41OWMxMi0yLjEwNSwyMC44OTktOC4yODUsMjguMDEtMTYuODI1ICBjLTAuNTA1LTAuNjU1LTEtMS4zMTUtMS41LTEuOTdjLTQuMTMsMS41NzQtOC4yNSwzLjE0NS0xMi44LDQuODc1YzMuMzUtNi44MjUsNi4yNzktMTIuNzk2LDkuMjEtMTguNzYxICBjLTAuNjAxLTAuMjctMS40NC0wLjU0LTIuMDQtMC44MWMtMy43NywyLjg4LTguOTIsNS43NS0xMy45MiwxMC40NWMwLTEwLjcxLDAtMTguMzYsMC0yNi4zN2MtNSw1LjQxLTEwLjMyLDEwLjc4LTE1LjU4NCwxNi4xNDkgIGMtMC40LTAuMzgtMC42OTEtMC43NjUtMS4xMDItMS4xNDljMi4xMDUtNS4zMTUsNC4yNjktMTAuNjMsNi42OTMtMTYuNzcxYy0yLjksMC40NDUtNC4zNTIsMC42NzYtNS43OTcsMC45ICBjLTAuNTIxLTQuNC0xLjAyMS04LjgxLTEuODctMTYuMDNjLTEwLjA5NSwxMi41LTE4LjUzLDIyLjk1LTI3LjMyLDMzLjgzOWMwLjYwNC0zLjU4NSwxLjEzLTYuNjg1LDEuNjU5LTkuNzg0ICBjLTE3LjcyOSwzNC44MDUtNDYuMzg1LDUwLjgyLTYzLjE4LDQwLjUzYy0wLjc2LDIuODQtMS41LDguODk2LTIuMjM0LDguODk2aC0yMS44NDVjMy4zLTI2LTExLjg0LTM5Ljc1Ni0zMS4zMTEtNTAuODA2ICBjLTEwLjYxOS02LjAyOS0yMi4wNjktMTAuODc4LTMyLjYwNC0xNy4wMzJjLTExLjA1LTYuNDU1LTIxLjkzOC0xMy41MjEtMzIuMDIyLTIxLjM2OWMtMTEuNTUtOC45ODctMjIuMTQ3LTE5LjI2NC0zMy4xMDItMjkuMDA5ICBjLTAuMjM1LTAuMjEsMC4xOTctMS4yMDQsMC4zMy0xLjgxM2MxMS43MzUsMC4zNDIsMTYuNTUsMy4zNDksMjQuMzQsMTUuMDkxYzEwLjI3NywxNS40ODcsMjQuNSwyNS4wMDUsNDMuNzcsMjkuNCAgYy03LjE2NS01LjAzOC0xMy4xNi05LjI1Ni0xOS4xNTItMTMuNDcxYzI2LjYxMiwxMC42NSw0Mi40NTIsMTMuODQxLDUxLjU2Miw4LjE4N2MtMy44OTktMC40ODUtNy42Ny0wLjk1NS05Ljk3NS0xLjI0MyAgYzguMTA0LTMuNDg1LDE2LjkyNS03LjI4LDI1Ljc0NS0xMS4wNzZjLTAuMzAxLTAuNjQtMC41OTYtMS4yODUtMC44OTYtMS45MjdjLTYuMDE1LDEuMDItMTIuMDI0LDIuMDM4LTE4LjA0NSwzLjA1OCAgYy0wLjI0LTAuODItMC40ODQtMS42NC0wLjcyOS0yLjQ1OGMyMy42MzUtOC41NiwzNS44MjktMjUuNjU1LDM5LjY5OS01Mi41MWMtMTIuMjUsMTkuMDYzLTE0LjQzLDIwLjYzLTI0LjU3OSwxOC45MjcgIGM2LjM0LTIwLjE5LDYuNjQ1LTI5LjQyMi0zLjIyMS0zNS4wNDVjMS41ODUsMTcuODgtMi4xNiwyMy4zMzctMTYuMSwyMy40NWMxLjUtMy45NTUsMy03LjkxMyw0LjMzLTExLjQwOCAgYy01LjA3LDIuMDMtOS41MywzLjgyLTE0LjM0LDUuNzQ4Yy0xLjE1LTUuODUtMS45MS05LjcyLTIuODYtMTQuNTdjLTE3LjA0NSwxOC45Ni0zMy45MTcsMjAuOTgtNTMuMDYsNi44MTcgIGM3LjM4NSwwLjYxLDEzLjk2NSwxLjE1MiwyMC41NSwxLjY5NWMtOC41ODctMy45Ny0xNy41OC01LjYwMi0yNi4zNzUtNy45NjdjLTMuMTY1LTAuODQ4LTcuOTE3LTMuMDA3LTguNDI1LTUuMzM1ICBjLTIuOTc4LTEzLjY1NS01LjQ0Ny0yNy40NzUtNy4wMS00MS4zNmMtMS4wNjUtOS40NDctMC40Ni0xOS4wOTctMC40MzUtMjguNjU1YzAuMDctMjkuMDYyLTIuNTc1LTU4Ljc1MiwxOC42MTUtODMuMTA5ICBjMC42NjMtMC43NiwwLjczMi0yLjA0LDEuMTYtMy4zMmMtMTYuMDYsNS40NDItMjYuMzMsMTYuMzA1LTMzLjk4MiwyOS45M2MtMy4yMzIsNS43NTctNi4yNTIsMTEuNjY3LTguOSwxNy43MSAgYy0xMy4zNDgsMzAuNDU5LTI0LjQ2Nyw2Mi4wMDctNDQuNSw4OS4xNTJjLTIuOTU4LDQuMDA3LTEuMTczLDYuMzYsMy44NjIsNy4yODJjOC4xMDcsMS40ODgsMTYuMDgsMy43MDUsMjQuMTEyLDUuNjEzICBjLTAuMTM1LDAuNTkyLTAuMjcyLDEuMTg4LTAuNDA4LDEuNzgyYy0yMC4zNjUtMy4zMjUtNDAuNzM0LTYuNjUyLTYxLjEtOS45OGMtMC4xNCwwLjc3NS0wLjI4LDEuNTUzLTAuNDIsMi4zMjggIGMxMC40MTMsMi43MSwyMC44MjIsNS40MTcsMjguNzIsNy40NzNjMTEuNjE4LDE1LjcwMi0xMC4wNjUsMTkuMTktOC44OTUsMzEuNjE3YzQuMDIyLTAuNTIyLDcuMDQ3LTAuOTE3LDEwLjIyLTEuMzMzICBjMi44MjUsMTAuOTMzLDUuNDg3LDIxLjI0OCw4LjM1MywzMi4zM2M4LjUwMi0xLjI1LDE5LjExNy00LjY2LDI0Ljg2LDUuMThjLTIuODU1LDEwLjItNi41NDIsMTguODQ1LTcuNTEsMjcuNzg1ICBjLTEuODE3LDE2LjgwNS01LjE1NSwzMi40Ni0xNy45NzcsNDQuNzZjLTYuNDM4LDYuMTgxLTExLjY0NywxNC4yNjYtMTguNTIsMTkuODM2Yy00LjY0MywzLjc2LTEwLjIxLDkuNjQ2LTE1LjkxMyw5LjY0NkgxNi41MiAgYy0zLjg0LDAtNi45NSwzLjY2LTYuOTUsNy41czMuMTEsNy41LDYuOTUsNy41aDg1LjI2NGM4LjEwOCwwLDE2LjE1NSwyLjA2LDI0LjU0LDEuMjJjNC4yNC0wLjQyLDguNDgyLTEuMjIsMTIuNzIzLTEuMjJoNDcuNDAyICBjLTUuODY1LDUtMTEuOTEsNi45MDgtMTcuNDEyLDExLjQ0OWMtMTAuMjYsOC40NzEtMTAuNjc1LDE0LjMxNy0yLjUzNSwyNC44MjJjNy40ODgsOS42NiwxNS41NzIsMTguNjc2LDIyLjE2NSwyOC45MTYgIGMzLjk1LDYuMTQsNy4yMTIsMTMuNTE1LDcuODczLDIwLjY4YzEuOTgsMjEuNDUsNS40NTgsMjMuODc2LDI1LjQ3NSwxNS4wMzZjMi40MTUtMS4wNjUsNC42MjUtMi43MDUsNy4xMDUtMy41MzUgIGMxLjk5My0wLjY2NSw0LjcwOC0xLjQ1LDYuMzEzLTAuNjNjNy40NzMsMy44MSwxNC42Nyw4LjE1NCwxOS45OTIsMTEuMTg5Yy03LjIyMiwxMi4yMDUtMTUuMjE3LDIyLjUzNS0xOS40OTIsMzQuMjIgIGMtMi4zMTUsNi4zMjUsMC45OTUsMTUuMiwzLjMyLDIyLjQzYzAuNzQsMi4zMDYsMy4yNDksNC40MzEsOS45MzUsNC40MzFjMy4zMTQtMC4wMDgsMi41LTUuNzgsMi44Ny04Ljc4ICBjMC40NC0zLjU2NC0wLjIzOC03LjI1NS0wLjE3NS0xMC44OWMwLjIyMy0xMi45NTUsMi45NzgtMTUuNzM1LDE1LjQtMTkuMDYxYzcuODY0LTIuMTA5LDE2LjM1OS00LjI3OSwyMi42NzUtOC45NyAgYzUuMTY1LTMuODQsMTAuODk1LTExLjgsMTAuMzQtMTcuMzE5Yy0wLjg2LTguNTg1LTEwLjg2NS03LjQ2LTE3Ljk0NS04LjIyNmMtMS42NC0wLjE3NS0zLjMzLDAuMDYxLTQuOTQ5LTAuMTc1ICBjLTguNDI2LTEuMjM1LTE0LTUuOTYtMTguMDctMTQuODY1YzguODMtMi4zODUsMTYuNzYtNC4zNTksMjQuNTg1LTYuNzA0YzQuMTMtMS4yMzUsMTAuODc1LTIuNDQsMTEuNDU1LTQuODUxICBjMC45Mi0zLjg4LTEuNDgtOS4yNzQtMy45NS0xMy4wNmMtMi42NC00LjAzLTYuODUtNy4xMTUtMTAuNjU1LTEwLjI4Yy0yLjkwNC0yLjQxLTYuMjM5LTQuMzA1LTkuMzg1LTYuNDMgIGMwLjgwNS0xLjAwNSwxLjYwNS0yLjAxLDIuNDEtMy4wMWMxNi4zNSwxLjYsMzEuMzQtMS42NSw0Mi42My0xMy45NGM1LjcyOS02LjI0LDkuNi0xMy4wODUsMTQuNjItMjAuMDI0ICBjMC44MTktMS4xMjksMS44LTIuNDI0LDIuODU5LTIuNDI0aDIxLjY1Yy0wLjk0LDAtMS43NSwzLjg4NS0yLjg4LDcuMTA0YzIuOTMtMS44NTQsNC4zOS0zLjMxNyw1Ljg1OS00LjI1MiAgYzEuMDYxLDEuNzg1LDIuMTEsMy4zMDYsMy40MzEsNS41NTZjMS42ODktMy4yODUsMi45Ny01Ljg5LDMuMDg1LTYuMTJjNi40OTQsMC43MTYsMTAuOTY0LDEuMjA2LDE4LjA0NCwxLjk4MSAgYzEuNTgsMi4yMjksMi45MSw2LjU0LDcuOTEsMTEuOTNjMC0zLjUxLDAtNS41NiwwLTcuNjA5YzAtMC41LDIuMzE0LTAuOTksMy4xNy0xLjQ5YzYuNjUsOS4zNCwxMy42MDUsMTguNjgsMTkuNTI1LDI3ICBjLTAuMTItMS4zOTEtMC4yMzctNC4zMzUtMC42OTItOS4zN2MxMS40MiwxNC45MzksMjEuNTc1LDI4LjEzLDMzLjI5NSw0My40N2MxLjI1LTcuMjEsMi4wMzYtMTEuNTYsMi43ODYtMTUuODkgIGMxLjQyNSwwLjMxOSwyLjg3MywwLjYxOSw1LjU5MywxLjE5OWMtMi43NC02Ljc3NC01LjEwNy0xMi42NC03LjQ4Mi0xOC41MWMxLTAuODgsMi4wMDUtMS43NjUsMy4wMDUtMi42NDkgIGM0Ljg1LDYuNDI1LDkuNzAxLDEyLjg1LDE0LjU1MSwxOS4yNzRjMC42MzUtNi42NjUtMy4yOTUtMTQuMjg1LTEuMTEtMTcuMDRjOC4xNDEtMTAuMjU1LDAuMDQtMTMuNjM1LTUuNzctMTguNDA5ICBjLTIuMDE2LTEuNjU1LTQuMjktMi45OS02LjQ0NS00LjQ3MWMwLjIyNi0wLjQ0OSwwLjQ0NS0wLjg5OSwwLjY2NS0xLjM1YzEwLjgyLDMuMTY1LDIxLjY0NSw2LjMyLDMyLjQ3LDkuNDc5ICBjMC40MjUtMC42OTQsMC44NTEtMS4zOTUsMS4yNzUtMi4wOWMtMy42NDYtMi45NjUtNy4yOS01LjkzNS0xMS4yMjYtOS4xMzVjOC4xNS0wLjU3LDE4LjM4LTguNDYsMjAuODgtMTYuNDEgIGMtMy4zMTktMC42ODktNi40ODktMS4zNS05LjY2NC0yLjAwNUM1MDEuMjY1LDMzNS4zMDQsNDk4LjEsMzIzLjAwOSw1MDEuNDMsMzE0LjU0OXogTTE2Ny45NzIsMzE2LjgxNCAgYzkuMjE3LDIuODYsMTcuOTU3LDUuODM4LDI2LjY5Nyw4LjU1M2MtMC4xMDMsMC43OS0wLjIwOCwyLjYzMy0wLjMxLDIuNjMzaC0zMi44MjVDMTYzLjc4NCwzMjMsMTY2LjAwNCwzMjAuMDc0LDE2Ny45NzIsMzE2LjgxNHoiLz48L3N2Zz4=";
    const svg = atob(dataUri
        .replace(/data:image\/svg\+xml;base64,/, ''))
        .replace(/(height=")(512)(px")/g, '$1550$3')
        .replace(/(clip-rule="evenodd")/g, '$1 class="moon"')

    document.querySelector('.first-window').innerHTML = svg
    const h1 = document.createElement('h1')
    h1.innerHTML = "Witches Series by Terry Pratchett"
    h1.className = "title"

    document.querySelector('.first-window').appendChild(h1)
    document.querySelector('.first-window').onclick = loadSecondWindow
}

const loadSecondWindow = () => {
    document.querySelector('.first-window').classList.add('slide-out')
    document.querySelector('.second-window').classList.add('show')
    document.querySelectorAll('.cover').forEach(item => item.onclick = loadBookInfo)
}

const loadBookInfo = async (event) => {
    document.querySelector('.second-window').classList.remove('show')
    document.querySelector('.second-window').classList.add('slide-out')
    document.querySelector('.third-window').classList.add('show')

    titleBook = event.target.parentNode.dataset.title
    const url = urlTemplate.replace(/titleBook/, titleBook)

    const info = await getInfo(url)
    
    const plot = info[Object.keys(info)].revisions[0]['*']
    const title = info[Object.keys(info)].title
    loadThirdWindow(plot, title)
}

const getInfo = async(url) => {
    return new Promise((resolve) => 
        fetch(url)
            .then((info) => {
                info.json()
                    .then(data => 
                        resolve(data.query.pages)
                    )
        })
    )
}

const loadThirdWindow = (plot, title) => {
    let content = plot
        .replace(/\r|\n/g, " ")
        .replace(/==\s*Plot[^=]*|Synopsis==/g, '')
        .replace(/''/g, '"')
        .replace(/\[\[([^|\]]*\|)?([^\]]*)\]\]/g, "$2")
        .split(/==|\s/)
        .slice(0, 400)
        .join(' ')
        .concat('...')

    const h2  = `<h2>${ title }</h2>`
    const div = `<div>${ content }</div>`
    document.querySelector(".third-window").innerHTML = h2.concat(div)
    const btn = document.createElement("button")
    btn.className = "back"
    btn.innerHTML = "⬅"
    btn.onclick = backToSecond
    document.querySelector(".third-window").appendChild(btn)
}

const backToSecond = () => {
    document.querySelector('.second-window').classList.remove('slide-out')
    document.querySelector('.third-window').classList.remove('show')
    loadSecondWindow()
}