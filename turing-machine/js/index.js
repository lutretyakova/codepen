
const ALPHABET = ['0', '1', '$', '#']
const CHAR_COUNT = 6;
const LOADED = 'loaded'
const WAIT = 'wait'

let isCounting = false
let line = []
let caretpos = 0

window.onload = () => {
    init()
}

setCaret = () => {
    const digitBlockWidth = 500 / 6 
    const caretWidth = 50

    const caretLeft = caretpos * digitBlockWidth + digitBlockWidth / 2 - caretWidth / 2
    
    document.querySelector('.caret').style.left = `${ caretLeft }px`
}

init = () => {
    isCounting = false
    line = []
    do {
        for (let inx = 0; inx < CHAR_COUNT; inx++) {
            const char = ALPHABET[Math.floor(Math.random() * ALPHABET.length)]
            line.push(char)
        }
    } while(line.filter(item => (item === '#' || item === '$')).length < 3)

    document.querySelectorAll('.digit').forEach((block, inx) => {
        window.setTimeout(() => {
            block.innerHTML = line[inx]
        }, 200 * inx)
    })
    
    caretpos = Math.floor(Math.random() * CHAR_COUNT)
    setCaret()
  
    setStatus(LOADED)
    window.setTimeout(() => {
        start()
    }, 3000)
}

reload = () => {
    document.querySelector('.status').innerHTML = WAIT
    if (isCounting) {
        return
    }
    init()
}

setStatus = (status, delay = 1200) => {
    window.setTimeout(() => {
        document.querySelector('.status').innerHTML = status
    }, delay)
}

start = () => {
    isCounting = true
    document.querySelectorAll('.digit').forEach((block, inx) => {
        window.setTimeout(() => {
            document.querySelector('.status').innerHTML = 'go to cell'
            setStatus('read cell', 1100)
            
            caretpos = inx
            setCaret()
            if ('$#'.includes(line[inx])) {
                setStatus('write 0 to cell', 1800)
                line[inx] = '0'
                window.setTimeout(() => {
                    block.innerHTML = line[inx]
                }, 1800)
            } else {
                setStatus('', 1800) 
            }
        }, 2500 * inx)
    })

    window.setTimeout(() => {
        document.querySelector('.status').innerHTML = 'done'
        document.querySelector('.caret').style.left = '475px'
        isCounting = false
    }, 2500 * CHAR_COUNT)
}