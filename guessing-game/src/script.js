let svgNode

window.onload = () => {
    svgNode =  document.getElementsByTagName('svg')

    document.getElementById('move-circle-light').beginElement()
    document.getElementById('move-circle-dark').beginElement()

    document.querySelector('.light').onclick = startGame
    document.querySelector('.dark').onclick = startGame
}

const startGame = (event) => {
    document.querySelector('.status').innerHTML = 'Waiting.....'

    let secondTime = Math.random() * 8.4
    document.querySelector(`.second-circle animateMotion`).setAttribute('dur', `${ secondTime }s`)

    let firstTime = Math.random() * 8.4
    document.querySelector(`.first-circle animateMotion`).setAttribute('dur', `${ firstTime }s`)
  
    if (document.querySelector('.selected')) {
        document.querySelector('.selected').classList.remove('selected')
    }
    
    let selectedNodeClass = event.currentTarget.classList
    selectedNodeClass.add('selected')

    document.getElementById('move-circle-light').beginElement()
    document.getElementById('move-circle-dark').beginElement()

    setTimeout(() => {
        if (firstTime < secondTime && selectedNodeClass.contains('dark') || 
                secondTime < firstTime && selectedNodeClass.contains('light')) {
            document.querySelector('.status').innerHTML = "It's correct!"
        } else {
            document.querySelector('.status').innerHTML = "Try again!"
        }
    }, Math.min(firstTime, secondTime) * 1000)
}