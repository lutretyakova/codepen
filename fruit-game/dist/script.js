const CELL_SIZE = 40

const WIDTH = 480
const HEIGHT = 520

const CELL_COUNT = WIDTH / CELL_SIZE * HEIGHT / CELL_SIZE
const OPEN_PER_TIME = 12

const FRUITS = ['apple', 'grapes', 'watermelon']
const VEGETABLES = ['carrot', 'paprica', 'pumpkin']
const FRUITS_AND_VEGETABLES = FRUITS.concat(VEGETABLES)

const OPEN_INTERVAL_SEC = 3

let timer = null
let counter = null
const totalCells = []
let currentCells = []
const picked = []

let counterInc = 0

window.onload = () => {
    for (let inx = 1; inx < CELL_COUNT + 1; inx++) { 
        const cell = document.createElement('div')
        cell.id = `id-${inx}`
        cell.className = 'cell'
        document.querySelector(".board").appendChild(cell);
    }

    getCells()

    timer = window.setInterval(() => {
        getCells()
        if (totalCells.length === CELL_COUNT) {
            window.clearInterval(timer) 
        }
    }, OPEN_INTERVAL_SEC * 1000)

    counter = window.setInterval(() => {
        const openTimes = OPEN_INTERVAL_SEC * CELL_COUNT / OPEN_PER_TIME - counterInc
        document.querySelector(`#counter`).innerHTML =  
            `00${ Math.trunc(openTimes / 60) }`.slice(-2) + 
            ':' +
            `00${ openTimes % 60 }`.slice(-2)
        counterInc++
        
        if (openTimes === 0) {
            window.clearInterval(counter) 
            checkGameResults()
        }
    }, 1000)
}

const hideAndShow = (current) => {
    current.forEach((inx) => {
        const puzzle = Math.floor(Math.random() * FRUITS_AND_VEGETABLES.length)
        const cellElem = document.querySelector(`#id-${ inx }`)
        cellElem.className = `cell ${FRUITS_AND_VEGETABLES[puzzle]}`
        cellElem.setAttribute('type', FRUITS_AND_VEGETABLES[puzzle])
        cellElem.setAttribute('number', inx)
        cellElem.addEventListener('click', clickCell)
    })
}

const clickCell = event => {
    document.querySelector(`#${ event.target.id }`).classList.add('clicked')
    const type = document.querySelector(`#${ event.target.id }`).getAttribute('type')
    if (VEGETABLES.includes(type)) {
        window.clearInterval(counter)
        window.clearInterval(timer) 
        document.querySelector('#status').innerHTML = 
            `${ type } is а vegetable`
    } else {
        picked.push(Number(event.target.getAttribute('number')))
    }
}

const getCells = () => {
    currentCells = []
    for (inx = 0; inx < OPEN_PER_TIME; inx++) {
        forOpen = 0
        do {
            forOpen = Math.ceil(Math.random() * CELL_COUNT)
        } while (totalCells.includes(forOpen))
        totalCells.push(forOpen)
        currentCells.push(forOpen)
    }
    hideAndShow(currentCells)
}

const checkGameResults = () => {
    const diff = totalCells.filter(item => {
        const type = document.querySelector(`#id-${ item }`).getAttribute('type')
        return FRUITS.includes(type) && picked.indexOf(item) < 0
    })
    
    if (diff.length) {
        diff.forEach(item => {
            document.querySelector(`#id-${ item}`).classList.add('missed')
            document.querySelector(`#id-${ item}`).removeEventListener('click', clickCell)
            document.querySelector('#status').innerHTML = 'You missed some'
        })
    } else {
        document.querySelector('#status').innerHTML = 'Greate game!' 
    }
}

window.onclose = () => {
    window.clearInterval(timer)
}