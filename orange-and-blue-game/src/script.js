const CELL_SIZE = 20
const BORDER = 10
const STEP = 5
const WIDTH = 600 * 0.8
const HEIGHT = 600
const ORANGE = '#f2642c'
const BLUE = '#347cc5'
const GAME_OVER = 'Game over!'
const TIME_FOR_SELECT = 2500
const MAX_FAIL = 5
const SCORE_INC = 3

const CELL_IN_WIDTH = WIDTH / CELL_SIZE
const CELL_IN_HEIGHT = HEIGHT / CELL_SIZE
const COLORS = [ORANGE, BLUE]
const LEFT = 37
const RIGHT = 39

let timer, timerGame

let newRect = []
let colorInx = 0
let direction = 0
let isGameOver = false

let sorted = []

let timerLost
let score = 0
let fail = 0

window.onload = () => {
    document.querySelector('.fail').textContent = `FAIL: ${ fail }/${ MAX_FAIL }`
    timeLost = TIME_FOR_SELECT
    generateNew()
    timer = setInterval(render, 1000 / 60)
    timerGame = setInterval(generateNew, 2000)
    window.focus()
}

const render = () => {
    const canvas = document.querySelector('canvas')
    const ctx = canvas.getContext('2d')

    ctx.clearRect(0, 0, WIDTH, HEIGHT)
    let [x, y] = newRect
    ctx.fillStyle = COLORS[colorInx]

    newRect = [x, y]
    ctx.fillRect(x, y, CELL_SIZE, CELL_SIZE)

    sorted = sorted.map(([[x, y], color, direction], _) => {
        switch (direction) {
            case LEFT:
                x = Math.max(BORDER, x - STEP)
                if (x === BORDER) {
                    direction = 0
                }
                break
            case RIGHT: 
                const maxX = WIDTH - BORDER - CELL_SIZE
                x = Math.min(maxX, x + STEP)
                if (x === maxX) {
                    direction = 0
                }
                break           
        }
        ctx.fillStyle = color
        ctx.fillRect(x, y, CELL_SIZE, CELL_SIZE)
        return[[x, y], color, direction]
    })

    timeLost = Math.round(timeLost - 1000 / 60)
    isGameOver = timeLost <= 0
    timeLost = Math.max(0, timeLost)
    document.querySelector('.time').textContent = `${ Math.floor(timeLost / 1000) }:${ timeLost % 1000}`  

    if (isGameOver) {
        finishGame()
    }
}

const generateNew = () => {
    const x = Math.min(Math.floor(Math.random() * WIDTH / 4) + WIDTH / 4, WIDTH - WIDTH / 4)
    const y = Math.min(Math.floor(Math.random() * HEIGHT) + BORDER, HEIGHT - 2 * CELL_SIZE)
    newRect = [x, y]
    colorInx = Math.round(Math.random())
    direction = 0
    if (timerLost > 0) {
        timeLost = TIME_FOR_SELECT
    } 
}

window.onkeydown = (event) => {
    if (isGameOver || (event.keyCode !== LEFT && event.keyCode !== RIGHT)) {
        console.log('return', isGameOver,event.keyCode !== LEFT,event.keyCode !== RIGHT)
        return
    }

    timeLost = TIME_FOR_SELECT
    switch (event.keyCode) {
        case LEFT:
            direction = LEFT
            if (COLORS[colorInx] === ORANGE) {
                score += SCORE_INC
            } else {
                fail++
            }
            break
        case RIGHT:
            direction = RIGHT; 
            if (COLORS[colorInx] === BLUE) {
                score += SCORE_INC
            } else {
                fail++
            }
            break
    }
    sorted.push([newRect, COLORS[colorInx], direction])

    document.querySelector('.fail').textContent = `FAIL: ${ fail }/${ MAX_FAIL }`
    document.querySelector('.score').textContent = `SCORE: ${ score }`

    if (fail === MAX_FAIL) {
        finishGame()
    }
    newRect = []
}

const finishGame = () => {
    document.querySelector('.status').textContent = GAME_OVER
    clearInterval(timerGame)
    clearInterval(timer)
}

window.onclose = () => {
    clearInterval(timerGame)
    clearInterval(timer)
}