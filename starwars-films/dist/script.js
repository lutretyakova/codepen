const FILM_URLS = [
    'https://swapi.co/api/films/4/',
    'https://swapi.co/api/films/5/',
    'https://swapi.co/api/films/6/',
    'https://swapi.co/api/films/1/',
    'https://swapi.co/api/films/2/',
    'https://swapi.co/api/films/3/',
    'https://swapi.co/api/films/7/'
]
const ICONS = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII']
const CHARACTER_ITEM_FOR_SHOW = 12
const SPECIES_ITEM_FOR_SHOW = 4
const ITEM_FOR_SHOW = 6
let current = 0

window.onload = async () => {
    let info = await getInfo(FILM_URLS[current])
    addInfo(info)

    document.querySelector('.next').addEventListener('click', getNext)
    document.querySelector('.back').addEventListener('click', getBack)
}

const getNext = async () => {
    current = current === ICONS.length - 1 
        ? 0
        : current + 1
    
    let info = await getInfo(FILM_URLS[current])
    addInfo(info)
}

const getBack = async () => {
    current = current === 0 
        ? ICONS.length - 1
        : current - 1
    
    let info = await getInfo(FILM_URLS[current])
    addInfo(info)
}

const getInfo = async(url) => {
    return new Promise((resolve) => 
        fetch(url)
            .then((info) => {
                resolve(info.json())
        })
    )
}

const insertData = (data, selector) => {
    document.querySelector(selector).innerHTML = ''
    data.forEach(async url => {
        const character = await getInfo(url)
        const block = document.createElement('li')
        block.className = 'name'
        block.innerHTML = character.name
        document.querySelector(selector).appendChild(block);
    })
}

const addInfo = (info) => {
    const title = info['title']
    document.querySelector('.title').innerHTML = title

    const iconInx = Number(info['episode_id']) - 1
    document.querySelector('.icon').innerHTML = ICONS[iconInx]

    const crawl = info['opening_crawl'].replace(/\r\n/g, ' ')
    document.querySelector('.crawl').innerHTML = crawl

    const director = info['director']
    document.querySelector('.director .name').innerHTML = director

    const date = info['release_date']
    document.querySelector('.year .name').innerHTML = date



    insertData(
        info['characters'].slice(0, CHARACTER_ITEM_FOR_SHOW),
        '.characters ul'
    )

    insertData(
        info['planets'].slice(0, ITEM_FOR_SHOW),
        '.planets ul'
    )

    insertData(
        info['starships'].slice(0, ITEM_FOR_SHOW),
        '.starships ul'
    )

    insertData(
        info['vehicles'].slice(0, ITEM_FOR_SHOW),
        '.vehicles ul'
    )

    insertData(
        info['species'].slice(0, SPECIES_ITEM_FOR_SHOW),
        '.species ul'
    )
}