const ARR_LENGTH = 70
const B_WIDTH = 700 / ARR_LENGTH
const indexes = [...Array(ARR_LENGTH).keys()].map(x => ++x);
const shuffle = () => indexes.sort(() => Math.random() - 0.5);

let counter = 0
window.onload = () => {
    shuffle()
    let x
    for (let inx of indexes)
    {
        const block = document.createElement('div')
        block.className = 'block'
        x = (inx - 1) % ARR_LENGTH 
        
        block.style.backgroundPositionX = `${ -x * B_WIDTH }px`

        block.setAttribute('id', `id-${inx}`)
        document.querySelector('.container').appendChild(block)
    }

    const replaceDivs = () => {
        let isSorted = true
        for (let inx = 0; inx < ARR_LENGTH; inx++)
        {
            if (indexes[inx] > indexes[inx + 1]) {
                const parent = document.querySelector(".container")
                const first = document.querySelector(`#id-${ indexes[inx + 1] }`)
                const second = document.querySelector(`#id-${ indexes[inx] }`)
                
                parent.insertBefore(first, second)
                const a = indexes[inx + 1]
                indexes[inx + 1] = indexes[inx]
                indexes[inx] = a 
                isSorted = false
                // [indexes[inx + 1], indexes[inx]] = [indexes[inx], indexes[inx + 1]]
            }
        }
        if (isSorted) {
            clearInterval(timer)
        }
    }

    const timer = setInterval(replaceDivs, 150)
}