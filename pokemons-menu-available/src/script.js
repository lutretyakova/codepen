const urls = {
    'scyther': 'https://pokeapi.co/api/v2/pokemon/123/',
    'dragonite': 'https://pokeapi.co/api/v2/pokemon/149/',
    'meowth': 'https://pokeapi.co/api/v2/pokemon/52/',
    'blastoise': 'https://pokeapi.co/api/v2/pokemon/9/',
    'pikachu': 'https://pokeapi.co/api/v2/pokemon/25/',
    'charizard': 'https://pokeapi.co/api/v2/pokemon/6/'
}
const DEFAULT_URL = urls['meowth']
const LIST_MAX_SIZE = 6

let current = 'meowth'

window.onload = async () => {
    let info = await getInfo(DEFAULT_URL)
    addInfo(info)

    document.querySelector('svg').addEventListener('click', activateMenu)
    document.querySelectorAll('.menu-item').forEach(
        item => item.addEventListener('click', loadNewInfo)
    )
}

const activateMenu = () => {
    if (document.querySelector('nav.active') !== null) {
        document.querySelector('nav').classList.remove('active')
        document.querySelector('svg').classList.remove('active')
    } else {
        document.querySelector('nav').classList.add('active') 
        document.querySelector('svg').classList.add('active') 
    }
}

const loadNewInfo = async(event) => {
    const name = event.target.className
        .replace(/menu/g, '')
        .replace(/active/g, '')
        .replace(/-item/g, '')
        .replace(/\s/g, '')
    
    if (current !== name) {
        document.querySelector(`.${ current }-item`).classList.remove('active')
        current = name
        let info = await getInfo(urls[current])
        addInfo(info)
        document.querySelector('nav').classList.remove('active')
        document.querySelector('svg').classList.remove('active')
    }
}

const getInfo = async(url) => {
    return new Promise((resolve) => 
        fetch(url)
            .then((info) => {
                resolve(info.json())
        })
    )
}

const insertLocationAreaData = async(url) => {
    const data = await getInfo(url)
    if (!data.length) {
        document.querySelector('.location ul').innerHTML = '<li>No data</li>'
        return
    }

    document.querySelector('.location ul').innerHTML = ''
    data
        .slice(0, LIST_MAX_SIZE)
        .forEach(location => {
            const name = location['location_area']['name']
                .replace(/-/g, ' ')

            const block = document.createElement('li')
            block.innerHTML = name
            document.querySelector('.location ul').appendChild(block)
        })
}

const insertAbilities = abilities => {
    document.querySelector('.abilities ul').innerHTML = ''
    abilities.forEach(ability => {
        const block = document.createElement('li')
        block.innerHTML = `${ ability['ability'].name } 
            ${ ability['is_hidden'] === true ? " (hidden)": ""}`

        document.querySelector('.abilities ul').appendChild(block)
    })   
}

const insertHeldItems = items => {
    if (!items.length) {
        document.querySelector('.held-items ul').innerHTML = '<li>No data</li>'
        return
    }

    document.querySelector('.held-items ul').innerHTML = ''
    items.forEach(item => {
        const block = document.createElement('li')
        block.innerHTML = item['item'].name
        document.querySelector('.held-items ul').appendChild(block)
    })
}

const insertForms = forms => {
    document.querySelector('.forms ul').innerHTML = ''
    forms.forEach(form => {
        const block = document.createElement('li')
        block.innerHTML = form.name
        document.querySelector('.forms ul').appendChild(block)
    })   
}

const insertTypes = types => {
    document.querySelector('.types ul').innerHTML = ''
    types.forEach(type => {
        const block = document.createElement('li')
        block.innerHTML = type.type.name
        document.querySelector('.types ul').appendChild(block)
    })   
}

const addInfo = (info) => {
    const name = info['name']
    document.querySelector('h1').innerHTML = name
    document.querySelector('.pokemon-avatar').className = `pokemon-avatar ${ name }`
    document.querySelector(`.${ name }-item`).classList.add('active')

    const order = info['order']
    document.querySelector('.order p').innerHTML = order

    const experience = info['base_experience']
    document.querySelector('.base-experience p').innerHTML = experience

    const height = info['height']
    document.querySelector('.height p').innerHTML = height

    const weight = info['weight']
    document.querySelector('.weight p').innerHTML = weight

    insertLocationAreaData(info['location_area_encounters'])

    const abilities = info.abilities.slice(0, LIST_MAX_SIZE)
    insertAbilities(abilities)

    const heldItems = info['held_items'].slice(0, LIST_MAX_SIZE)
    insertHeldItems(heldItems)

    const forms = info['forms'].slice(0, LIST_MAX_SIZE)
    insertForms(forms)

    const types = info['types'].slice(0, LIST_MAX_SIZE)
    insertTypes(types)
}
